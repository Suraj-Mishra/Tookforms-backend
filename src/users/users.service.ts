import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { UserDto } from './dto/create-test.dto';
import { UserSchema, userDocument } from './schemas/create-test.schema';
import { InjectModel } from '@nestjs/mongoose';
// import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel('Post') private readonly userModel: Model<userDocument>,
  ) {}

  async getAllTests() {
    const tests = await this.userModel.find().exec();
    return tests;
  }

  // saveCandidateAnswer(saveCandidateAnswerDto: saveCandidateAnswerDto) {
  // }

  async createTest(User: UserDto): Promise<UserDto> {
    const createdUser = new this.userModel(User);
    return createdUser.save();
  }

  async updateTest(User: UserDto): Promise<UserDto> {
    const getUser = new this.userModel(User);
    return getUser;
  }

  // createTestAnswer(createTestAnswerDto: createTestAnswerDto) {
  // }

  // updateTestAnswer(updateTestDto: updateTestDto) {
  // }

  // updateTestCount(updateTestCountDto: updateTestCountDto) {
  // }

  // updateTestDataDto(updateTestDataDto: UpdateTestDataDto) {

  // create(createUserDto: CreateUserDto) {
  //   return 'This action adds a new user';
  // }

  // findAll() {
  //   return `This action returns all users`;
  // }

  // findOne(id: number) {
  //   return `This action returns a #${id} user`;
  // }

  // update(id: number, updateUserDto: UpdateUserDto) {
  //   return `This action updates a #${id} user`;
  // }

  // remove(id: number) {
  //   return `This action removes a #${id} user`;
  // }
}
