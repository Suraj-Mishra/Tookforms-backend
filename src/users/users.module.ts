import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { MongooseModule } from '@nestjs/mongoose';
// import { TestContainer, TestContainerSchema } from './schemas/create-test.schema';
import { UserSchema } from './schemas/create-test.schema';
@Module({
  imports: [MongooseModule.forFeature([{ name: 'Post', schema: UserSchema }])],
  controllers: [UsersController],
  providers: [UsersService],
})
export class UsersModule {}
