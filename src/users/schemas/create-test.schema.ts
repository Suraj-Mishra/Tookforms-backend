import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { UserAnswer, IsKeyValuePairValidate } from './../dto/create-test.dto';

@Schema()
class testInfo {
  @Prop({ required: true })
  readonly title: string;

  @Prop({ required: true })
  readonly score: number;

  @Prop({ required: true })
  readonly description: string;

  @Prop({ required: true })
  readonly time: string;

  @Prop({ required: true })
  readonly dateOfCreation: string;
}

const testInfoSchema = SchemaFactory.createForClass(testInfo);

@Schema()
class OptionContent {
  @Prop({ UserAnswer, required: true })
  readonly answer: UserAnswer;

  @Prop({ required: true })
  readonly description: string;

  @Prop({ required: true })
  readonly option: string;
}

const OptionContentSchema = SchemaFactory.createForClass(OptionContent);

@Schema()
class Options {
  @Prop({ type: [OptionContentSchema], default: [], required: true })
  readonly option: OptionContent[];
}

const OptionsSchema = SchemaFactory.createForClass(Options);

@Schema()
class Page {
  @Prop({ required: true })
  readonly question: string;

  @Prop({ type: [OptionsSchema], default: [], required: true })
  readonly options: Options[];
}

const PageSchema = SchemaFactory.createForClass(Page);

@Schema()
export class Test {
  @Prop({ type: testInfoSchema, required: true })
  readonly info: testInfo;

  @Prop({ type: [PageSchema], default: []. required: true })
  readonly McqStore: Page[];
}

const TestSchema = SchemaFactory.createForClass(Test);

@Schema()
export class TestContainer {
  @Prop({ required: true })
  readonly testName: string;

  @Prop({ type: TestSchema, required: true })
  readonly data: Test;
}

const TestContainerSchema = SchemaFactory.createForClass(TestContainer);

@Schema()
export class User {
  @Prop({ required: true })
  readonly email: string;

  @Prop({ type: [TestContainerSchema], default: [], required: true })
  readonly data: TestContainer[];
}

export const UserSchema = SchemaFactory.createForClass(User);

export type userDocument = User & mongoose.Document;
