import { Type } from 'class-transformer';
import {
  IsString,
  IsNotEmpty,
  ValidateNested,
  IsNumber,
  IsEnum,
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  Validate,
  ValidationOptions,
  registerDecorator,
} from 'class-validator';

// @ValidatorConstraint({ name: 'customText', async: false })
// export class IsKeyValuePairValidate implements ValidatorConstraintInterface {
// 	validate(columnValue: Object, args: ValidationArguments): boolean {
// 		console.log("obj ====> ", typeof args.property === 'string');
// 		console.log("col =====> ", columnValue)
// 		const data1 = Object.keys(args.object).filter((item) => item !== "");
// 		const data2 = Object.keys(args.object);
// 		console.log(data1.length, data2.length)
// 		// console.log(data1);
// 		if (typeof args.property === 'string' && data1.length === data2.length) {
// 			console.log(true);
// 			return true;
// 		} else {
// 			console.log(false);
// 			return false;
// 		}
// 	}
// }

export function IsKeyValuePairValidate(
  property: string,
  validationOptions?: ValidationOptions,
) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isLongerThan',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [property],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          console.log('value =====> ', value);
          console.log('args =====> ', args);
          console.log(typeof args.property === 'string');
          console.log(args.property.length);
          if (typeof args.property === 'string' && args.property.length > 0) {
            console.log(true);
            return true;
          } else {
            console.log(false);
            return false;
          }
        },
      },
    });
  };
}

export enum UserAnswer {
  correct = 'correct',
  wrong = 'wrong',
}

class Info {
  @IsNotEmpty()
  @IsString()
  readonly title: string;

  @IsNotEmpty()
  @IsNumber()
  readonly score: number;

  @IsNotEmpty()
  @IsString()
  readonly description: string;

  @IsNotEmpty()
  @IsString()
  readonly dateOfCreation: string;

  @IsNotEmpty()
  @IsString()
  readonly time: string;
}

class OptionItem {
  @IsNotEmpty()
  @IsEnum(UserAnswer)
  readonly answer: UserAnswer;

  @IsNotEmpty()
  @IsString()
  readonly description: string;

  @IsNotEmpty()
  @IsString()
  readonly option: string;
}

class Option {
  @ValidateNested({ each: true })
  @IsNotEmpty()
  @Type(() => OptionItem)
  readonly option: OptionItem[];
}

class Options {
  @ValidateNested({ each: true })
  @IsNotEmpty()
  @Type(() => Option)
  readonly options: Option[];

  @IsNotEmpty()
  @IsString()
  readonly question: string;
}

export class TestDto {
  @ValidateNested()
  @IsNotEmpty()
  @Type(() => Info)
  readonly info: Info;

  @ValidateNested({ each: true })
  @IsNotEmpty()
  @Type(() => Options)
  readonly McqStore: Options[];
}

export class CreateTestDto {
  @IsNotEmpty()
  @IsString()
  // @IsKeyValuePairValidate('something', { message: 'no can do' })
  readonly testName: string;

  @ValidateNested()
  @IsNotEmpty()
  @Type(() => TestDto)
  readonly data: TestDto;
}

export class UserDto {
  @IsString()
  // @Validate(IsKeyValuePairValidate, { message: 'no can do' })
  // @IsKeyValuePairValidate('something', { message: 'no can do' })
  @IsNotEmpty()
  readonly email: string;

  @ValidateNested({ each: true })
  @IsNotEmpty()
  @Type(() => CreateTestDto)
  readonly data: CreateTestDto[];
}
