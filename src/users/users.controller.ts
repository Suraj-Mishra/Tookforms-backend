import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UserDto } from './dto/create-test.dto';
// import { UpdateUserDto } from './dto/update-test.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  // @Get('/results')
  // getResults() {
  //   return this.usersService.getResults()
  // }

  // @Post('/saveCandidateAnswer')
  // saveCandidateAnswer(@Body() saveCandidateAnswerDto: saveCandidateAnswerDto) {
  //   return this.usersService.saveCandidateAnswer(saveCandidateAnswerDto)
  // }

  @Post('/createTest')
  createTest(@Body() User: UserDto) {
    console.log(User);
    return this.usersService.createTest(User);
  }

  // @Post('/createTestAnswer')
  // createTestAnswer(@Body() createTestAnswerDto: createTestAnswerDto) {
  //   return this.usersService.createTestAnswer(createTestAnswerDto);
  // }

  // @Patch('/updateTestAnswers')
  // updateTestAnswer(@Body() updateTestDto: updateTestDto) {
  //   return this.usersService.updateTest(updateTestDto);
  // }

  // @Patch('/updateTestCount')
  // updateTestCount(@Body() updateTestCountDto: updateTestCountDto) {
  //   return this.usersService.updateTestCount(updateTestCountDto)
  // }

  // @Patch('/updateTestDataDto')
  // updateTestDataDto(@Bidy() updateTestDataDto: UpdateTestDataDto) {
  //   return this.usersService.updateTestDataDto(updateTestDataDto)
  // }

  // @Get()
  // findAll() {
  //   return this.usersService.findAll();
  // }

  // @Get(':id')
  // findOne(@Param('id') id: string) {
  //   return this.usersService.findOne(+id);
  // }

  // @Patch(':id')
  // update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
  //   return this.usersService.update(+id, updateUserDto);
  // }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.usersService.remove(+id);
  // }
}
